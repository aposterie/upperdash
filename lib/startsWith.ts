export default function startsWith(string: string, target: string, position?: number) {
	return string.substr(position || 0, target.length) === target;
}
