import slice from './slice';

// Delays a function for the given number of milliseconds, and then calls
// it with the arguments supplied.
export default function delay(func, wait) {
	let args = slice(arguments, 2);
	return setTimeout(function () {
		return func.apply(null, args);
	}, wait);
}
