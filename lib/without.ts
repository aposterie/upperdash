import slice from './slice';
import difference from './difference';

// Return a version of the array that does not contain the specified value(s).
export default function without(array: any[]): any[] {
	return difference(array, slice(arguments, 1));
}
