import isFunction from './isFunction';

// If the value of the named `property` is a function then invoke it with the
// `object` as context; otherwise, return it.
export default function result(object, property, fallback?) {
	let value = object == null ? undefined : object[property];
	if (value === undefined) {
		value = fallback;
	}
	return isFunction(value) ? value.call(object) : value;
}
