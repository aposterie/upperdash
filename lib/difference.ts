import filter from './filter';
import contains from './contains';
import baseFlatten from './_baseFlatten';

// Take the difference between one array and a number of other arrays.
// Only the elements present in just the first array will remain.

export default function difference(array: any[], ...others: any[][]): any[];

export default function difference(array: any[]) {
	let rest = baseFlatten(arguments, true, true, 1);
	return filter(array, value => !contains(rest, value));
}
