import keys from './keys';

// Returns whether an object has a given set of `key:value` pairs.
export default function isMatch(object, attrs): boolean {
	const names = keys(attrs);
	const length = names.length;
	if (object == null) return !length;
	const obj = Object(object);
	for (let i = 0; i < length; i++) {
		const key = names[i];
		if (attrs[key] !== obj[key] || !(key in obj)) return false;
	}
	return true;
}
