import defaultPlaceholder from './placeholder';
import slice from './slice';
import executeBound from './_executeBound';

export default function partial(func: Function): Function;
export default function partial(func: Function, ...args: any[]): Function;

// Partially apply a function by creating a version that has had some of its
// arguments pre-filled, without changing its dynamic `this` context. _ acts
// as a placeholder, allowing any combination of arguments to be pre-filled.
export default function partial(func) {
	let boundArgs = slice(arguments, 1);
	let bound = function () {
		let position = 0;
		const length = boundArgs.length;
		const args = Array(length);
		const placeholder = partial['placeholder'] || defaultPlaceholder;
		for (let i = 0; i < length; i++) {
			args[i] = boundArgs[i] === placeholder ? arguments[position++] : boundArgs[i];
		}
		while (position < arguments.length) args.push(arguments[position++]);
		return executeBound(func, bound, this, this, args);
	};
	return bound;
}
