import baseCreate from './_baseCreate';
import isObject from './isObject';

// Determines whether to execute a function as a constructor
// or a normal function with the provided arguments
export default function executeBound(sourceFunc, boundFunc, context, callingContext, args) {
	if (!(callingContext instanceof boundFunc)) {
		return sourceFunc.apply(context, args);
	}
	let self = baseCreate(sourceFunc.prototype);
	let result = sourceFunc.apply(self, args);
	if (isObject(result)) return result;
	return self;
}
