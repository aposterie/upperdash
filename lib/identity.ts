// Keep the identity function around for default iteratees.
export default function identity<T>(value: T): T {
	return value;
}
