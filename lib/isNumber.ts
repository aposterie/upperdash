import toType from './_toType';

export default function isNumber(obj: any): boolean {
	return typeof obj === 'number' || toType(obj) === 'Number';
}
