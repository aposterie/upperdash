import isObject from './isObject';
import has from './has';

// Retrieve the names of an object's own properties.
export default function keys(obj: any): string[] {
	if (!isObject(obj)) return [];
	let keys: string[] = [];
	for (let key in obj) {
		if (has(obj, key)) {
			keys.push(key);
		}
	}
	return keys;
}
