import isNumber from './isNumber';

// Is the given value `NaN`? (NaN is the only number which does not equal itself).
export default function (obj: any): boolean {
	return isNumber(obj) && isNaN(obj);
}
