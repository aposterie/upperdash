import has from './has';
import group from './_group';

// Groups the object's values by a criterion. Pass either a string attribute
// to group by, or a function that returns the criterion.
export default group((result, value, key) => {
	if (has(result, key)) {
		result[key].push(value);
	} else {
		result[key] = [value];
	}
});
