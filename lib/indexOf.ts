import findIndex from './findIndex';
import sortedIndex from './sortedIndex';
import createIndexFinder from './_createIndexFinder';

export default createIndexFinder(1, findIndex, sortedIndex);
