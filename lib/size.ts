import keys from './keys';
import isArrayLike from './isArrayLike';

// Return the number of elements in an object.
export default function size(obj): number {
	if (obj == null) return 0;
	return isArrayLike(obj) ? obj.length : keys(obj).length;
}
