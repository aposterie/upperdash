import delay from './delay';
import partial from './partial';
import placeholder from './placeholder';

// Defers a function, scheduling it to run after the current call stack has
// cleared.
export default partial(delay, placeholder, 1);
