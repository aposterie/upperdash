import isObject from './isObject';

// Retrieve all the property names of an object.
export default function allKeys(obj: Object): string[] {
	if (!isObject(obj)) return [];
	let keys = [];
	for (let key in obj) keys.push(key);
	return keys;
}
