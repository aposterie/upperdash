import keys from './keys';
import isArray from './isArray';
import isString from './isString';
import isArrayLike from './isArrayLike';
import isArguments from './isArguments';

// Is a given array, string, or object empty?
// An "empty" object has no enumerable own-properties.
export default function isEmpty(obj) {
	if (obj == null) {
		return true;
	} else if (isArrayLike(obj) && (isArray(obj) || isString(obj) || isArguments(obj))) {
		return obj.length === 0;
	} else {
		return keys(obj).length === 0;
	}
}
