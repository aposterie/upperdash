import has from './has';
import group from './_group';

// Indexes the object's values by a criterion, similar to `groupBy`, but for
// when you know that your index values will be unique.
export default group((result, value, key) => {
	result[key] = value;
});
