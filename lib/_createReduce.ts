import keys from './keys';
import isArrayLike from './isArrayLike';
import optimizeCb from './_optimizeCb';

// Create a reducing function iterating left or right.
export default function createReduce(dir) {
	// Optimized iterator function as using arguments.length
	// in the main function will deoptimize the, see underscore #1991.
	function iterator(obj, iteratee, memo, keys, index, length) {
		for (; index >= 0 && index < length; index += dir) {
			const currentKey = keys ? keys[index] : index;
			memo = iteratee(memo, obj[currentKey], currentKey, obj);
		}
		return memo;
	}

	return function (obj, iteratee, memo, context?) {
		iteratee = optimizeCb(iteratee, context, 4);
		const names = !isArrayLike(obj) && keys(obj);
		const length = (names || obj).length;
		let index = dir > 0 ? 0 : length - 1;
		// Determine the initial value if none is provided.
		if (arguments.length < 3) {
			memo = obj[names ? names[index] : index];
			index += dir;
		}
		return iterator(obj, iteratee, memo, names, index, length);
	};
}
