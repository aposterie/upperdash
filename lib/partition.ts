import cb from './_cb';
import each from './each';

// Split a collection into two arrays: one whose elements all satisfy the given
// predicate, and one whose elements all do not satisfy the predicate.
export default function partition(obj, predicate, context?) {
	predicate = cb(predicate, context);
	const pass = [], fail = [];
	each(obj, function (value, key, obj) {
		(predicate(value, key, obj) ? pass : fail).push(value);
	});
	return [pass, fail];
}
