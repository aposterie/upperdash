// String Functions
// -----------------
const nonWordChars = '([^\\w]|_)';
const caseConversionRegEx = RegExp(nonWordChars + '(\\w)', 'g');

function trimNonWordChars(str: string) {
	return ('' + str)
		.replace(RegExp('^' + nonWordChars + '+'), '')
		.replace(RegExp(nonWordChars + '+$'), '');
}

export default function createCaseConverter(replacer: (letter: string) => string) {
	return function (str: string) {
		if (!str) return '';
		return trimNonWordChars(str)
			.replace(caseConversionRegEx, function (match, dash, letter) {
				return replacer(letter);
			});
	}
}
