import slice from './slice';
import pullAll from './pullAll';

export default function pull(array: any[], ...values: any[]): any[];

export default function pull(array: any[]) {
	return pullAll(array, slice(arguments, 1));
}
