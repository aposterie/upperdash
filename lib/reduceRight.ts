import createReduce from './_createReduce';

// The right-associative version of reduce, also known as `foldr`.
export default createReduce(-1);
