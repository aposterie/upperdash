// Internal function that returns an efficient (for current engines) version
// of the passed-in callback, to be repeatedly applied in other Underscore
// functions.
export default function optimizeCb(func, context?, argCount?: number) {
	if (context === undefined) return func;
	switch (argCount == null ? 3 : argCount) {
		case 1: return function (value) {
			return func.call(context, value);
		};
		case 2: return function (value, other) {
			return func.call(context, value, other);
		};
		case 3: return function (value, index, collection) {
			return func.call(context, value, index, collection);
		};
		case 4: return function (accumulator, value, index, collection) {
			return func.call(context, accumulator, value, index, collection);
		};
		default: return function () {
			return func.apply(context, arguments);
		};
	}
}
