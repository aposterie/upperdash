import cb from './_cb';
import filter from './filter';
import negate from './negate';

// Return all the elements for which a truth test fails.
export default function reject(obj, predicate, context?) {
	return filter(obj, negate(cb(predicate)), context);
}
