import isObject from './isObject';
import isArray from './isArray';
import slice from './slice';
import extend from './extend';

// Create a (shallow-cloned) duplicate of an object.
export default function clone(obj) {
	if (!isObject(obj)) return obj;
	return isArray(obj) ? slice.call(obj) : extend({}, obj);
}
