import keys from './keys';

// Invert the keys and values of an object. The values must be serializable.
export default function invert(obj) {
	let result = {};
	let names = keys(obj);
	for (let i = 0, length = names.length; i < length; i++) {
		result[obj[names[i]]] = names[i];
	}
	return result;
}
