import cb from './_cb';
import keys from './keys';
import isArrayLike from './isArrayLike';

// Determine whether all of the elements match a truth test.
// Alased as `all`.
export default function every(obj, predicate, context) {
	predicate = cb(predicate, context);
	const names = !isArrayLike(obj) && keys(obj),
		length = (names || obj).length;
	for (let index = 0; index < length; index++) {
		let name = names ? names[index] : index;
		if (!predicate(obj[name], name, obj)) return false;
	}
	return true;
}
