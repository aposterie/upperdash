import getLength from './_getLength';
import slice from './slice';

// Generator function to create the indexOf and lastIndexOf functions
export default function createIndexFinder(dir: number, predicateFind: Function, sortedIndex?) {
	return function (array: any[], item, idx: number): number {
		let i = 0, length = getLength(array);
		if (typeof idx == 'number') {
			if (dir > 0) {
				i = idx >= 0 ? idx : Math.max(idx + length, i);
			} else {
				length = idx >= 0 ? Math.min(idx + 1, length) : idx + length + 1;
			}
		} else if (sortedIndex && idx && length) {
			idx = sortedIndex(array, item);
			return array[idx] === item ? idx : -1;
		}

		if (item !== item) {
			idx = predicateFind(slice(array, i, length), isNaN);
			return idx >= 0 ? idx + i : -1;
		}
		for (idx = dir > 0 ? i : length - 1; idx >= 0 && idx < length; idx += dir) {
			if (array[idx] === item) return idx;
		}
		return -1;
	};
}
