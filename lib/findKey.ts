import cb from './_cb';
import keys from './keys';

// Returns the first key on an object that passes a predicate test
export default function findKey(obj, predicate, context) {
	predicate = cb(predicate, context);
	let names = keys(obj), name;
	for (let i = 0, length = names.length; i < length; i++) {
		name = names[i];
		if (predicate(obj[name], name, obj)) return name;
	}
}
