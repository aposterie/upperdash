import slice from './slice';

// Returns everything but the last entry of the array. Especially useful on
// the arguments object. Passing **n** will return all the values in
// the array, excluding the last N.
export default function initial(array: any[], n: number, guard?) {
	return slice(
		array, 0,
		Math.max(0, array.length - (n == null || guard ? 1 : n))
	);
}
