import cb from './_cb';
import each from './each';

// An internal function used for aggregate "group by" operations.
export default function group(behavior) {
	return function (obj, iteratee, context) {
		const result = {};
		iteratee = cb(iteratee, context);
		each(obj, (value, index) => {
			let key = iteratee(value, index, obj);
			behavior(result, value, key);
		});
		return result;
	};
}
