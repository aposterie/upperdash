import has from './has';

// Memoize an expensive function by storing its results.
export default function memoize<T>(func: () => T, hasher?): (...args) => T {
	function memoized(key): T {
		const cache = memoized['cache'];
		const address = '' + (hasher ? hasher.apply(this, arguments) : key);
		if (!has(cache, address)) {
			cache[address] = func.apply(this, arguments);
		}
		return cache[address];
	}
	memoized['cache'] = {};
	return memoized;
}
