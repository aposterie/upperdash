import toType from './_toType';
import isObjectLike from './isObjectLike';

export default function isError(obj) {
	return isObjectLike(obj) && toType(obj) === 'Error';
}
