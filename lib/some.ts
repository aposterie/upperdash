import cb from './_cb';
import keys from './keys';
import isArrayLike from './isArrayLike';

// Determine if at least one element in the object matches a truth test.
// Aliased as `any`.
export default function some(obj, predicate, context?): boolean {
	predicate = cb(predicate, context);
	const names = !isArrayLike(obj) && keys(obj);
	const length = (names || obj).length;

	for (let index = 0; index < length; index++) {
		const currentKey = names ? names[index] : index;
		if (predicate(obj[currentKey], currentKey, obj)) return true;
	}
	return false;
}
