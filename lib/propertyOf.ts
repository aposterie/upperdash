import constant from './constant';

// Generates a function for a given object that returns a given property.
export default function propertyOf(obj: any) {
	return obj == null
		? constant(undefined)
		: (key: string|symbol) => obj[key];
}
