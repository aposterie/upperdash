// Is a given variable an object?
export default function isObject(obj: any): boolean {
	const type = typeof obj;
	return !!obj && (type === 'function' || type === 'object');
}
