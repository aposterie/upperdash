import cb from './_cb';
import keys from './keys';

// Returns the results of applying the iteratee to each element of the object
// In contrast to `map` it returns an object
export default function mapObject(obj, iteratee, context?) {
	iteratee = cb(iteratee, context);
	const names = keys(obj);
	const length = names.length;
	const results = {};

	for (let index = 0, name; index < length; index++) {
		name = names[index];
		results[name] = iteratee(obj[name], name, obj);
	}
	return results;
}
