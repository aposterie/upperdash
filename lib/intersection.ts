import contains from './contains';
import getLength from './_getLength';

// Produce an array that contains every item shared between all the
// passed-in arrays.
export default function intersection(array: any[]): any[] {
	let result = [];
	let argsLength = arguments.length;
	for (let i = 0, j, length = getLength(array); i < length; i++) {
		const item = array[i];
		if (contains(result, item)) continue;
		for (j = 1; j < argsLength; j++) {
			if (!contains(arguments[j], item)) break;
		}
		if (j === argsLength) {
			result.push(item);
		}
	}
	return result;
}
