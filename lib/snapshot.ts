import isObject from './isObject';
import has from './has';

// custom: underscore-contrib | underscore.object.builders.js#L67
export default function snapshot(obj) {
	if (!isObject(obj)) return obj;
	const temp = new obj.constructor();
	for (let key in obj) {
		if (has(obj, key)) temp[key] = snapshot(obj[key]);
	}
	return temp;
}
