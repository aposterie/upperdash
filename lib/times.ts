import optimizeCb from './_optimizeCb';

// Run a function **n** times.
export default function times(n: number, iteratee: (count?: number) => any, context?): any[] {
	const accum = Array(Math.max(0, n));
	iteratee = optimizeCb(iteratee, context, 1);
	for (let i = 0; i < n; i++) {
		accum[i] = iteratee(i);
	}
	return accum;
}
