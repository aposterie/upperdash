import cb from './_cb';
import each from './each';
import values from './values';
import isArrayLike from './isArrayLike';

// Return the minimum element (or element-based computation).
export default function min(obj, iteratee, context) {
	let result = Infinity, lastComputed = Infinity,	value, computed;

	if ((iteratee == null || typeof iteratee === 'number') && obj != null) {
		obj = isArrayLike(obj) ? obj : values(obj);
		const type = typeof obj[0];
		for (let i = 0, length = obj.length; i < length; i++) {
			value = obj[i];
			if (typeof value === type && value < result) {
				result = value;
			}
		}
	} else {
		iteratee = cb(iteratee, context);
		each(obj, (value, index, list) => {
			computed = iteratee(value, index, list);
			if (computed < lastComputed || computed === Infinity && result === Infinity) {
				result = value;
				lastComputed = computed;
			}
		});
	}
	return result;
}
