import createEscaper from './_createEscaper';
import escapeMap from './_escapeMap';
import invert from './invert';

const unescapeMap = invert(escapeMap);
export default createEscaper(unescapeMap);
