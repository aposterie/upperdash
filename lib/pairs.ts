import keys from './keys';

type Pair = [string|symbol, any];

// Convert an object into a list of `[key, value]` pairs.
export default function pairs(obj: Object): Pair[] {
	const names = keys(obj);
	const length = names.length;
	const pairs: Pair[] = Array(length);
	for (let i = 0; i < length; i++) {
		pairs[i] = [names[i], obj[names[i]]];
	}
	return pairs;
}
