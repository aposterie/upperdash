export default function toType(obj: any): string {
	return Object.prototype.toString.call(obj).slice(8, -1);
}
