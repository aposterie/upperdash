import isArrayLike from './isArrayLike';
import indexOf from './indexOf';
import values from './values';

// Determine if the array or object contains a given item (using `===`).
// Aliased as `includes`.
export default function contains(obj, item, fromIndex?: number, guard?) {
	if (!isArrayLike(obj)) obj = values(obj);
	if (typeof fromIndex != 'number' || guard) fromIndex = 0;
	return indexOf(obj, item, fromIndex) >= 0;
}
