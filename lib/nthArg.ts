export default function nthArg(num: number) {
	return function () {
		return arguments[num];
	};
}
