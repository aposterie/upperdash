import isObjectLike from './isObjectLike';
import toType from './_toType';

export default function isRegExp(obj: any): boolean {
	return isObjectLike(obj) && toType(obj) === 'RegExp';
}
