import toType from './_toType';
import isObjectLike from './isObjectLike';

export default function isArguments(obj) {
	return isObjectLike(obj) && toType(obj) === 'Arguments';
}
