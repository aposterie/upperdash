import toType from './_toType';

export default function isSymbol(obj: any): boolean {
	return typeof obj === 'symbol' || toType(obj) === 'Symbol';
}
