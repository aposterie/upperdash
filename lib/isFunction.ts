import toType from './_toType';

// Optimize `isFunction` if appropriate. Work around some typeof bugs in old v8,
// IE 11 (underscore #1621), and in Safari 8 (underscore #1929).
export default
	(typeof /./ != 'function' && typeof Int8Array != 'object')
	? (obj: any) => typeof obj == 'function'
	: (obj: any) => toType(obj) === 'Function';
