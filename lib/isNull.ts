// Is a given value equal to null?
export default function isNull(obj: any): boolean {
	return obj === null;
}
