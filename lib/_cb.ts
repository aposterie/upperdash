import matcher from './matcher';
import identity from './identity';
import isObject from './isObject';
import property from './property';
import optimizeCb from './_optimizeCb';
import isFunction from './isFunction';

// A mostly-internal function to generate callbacks that can be applied
// to each element in a collection, returning the desired result — either
// identity, an arbitrary callback, a property matcher, or a property accessor.
export default function cb(value: any, context?, argCount?): Function {
	if (value == null) {
		return identity;
	} else if (isFunction(value)) {
		return optimizeCb(value, context, argCount);
	} else if (isObject(value)) {
		return matcher(value);
	} else {
		return property(value);
	}
}
