export default function subtract(minuend: number, subtrahend: number) {
	return (minuend || 0) - (subtrahend || 0);
}
