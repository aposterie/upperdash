import map from './map';
import pick from './pick';
import negate from './negate';
import contains from './contains';
import isFunction from './isFunction';
import baseFlatten from './_baseFlatten';

// Return a copy of the object without the blacklisted properties.
export default function omit(obj, iteratee, context?) {
	// Babel bug: https://phabricator.babeljs.io/T7198
	// TODO: Remove after Babel 6.7 release.
	let keys;
	if (isFunction(iteratee)) {
		iteratee = negate(iteratee);
	} else {
		keys = map(baseFlatten(arguments, false, false, 1), String);
		iteratee = function (value, key) {
			return !contains(keys, key);
		};
	}
	return pick(obj, iteratee, context);
}
