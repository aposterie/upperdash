import slice from './slice';

export default function chunk(array, count) {
	if (count == null || count < 1) return [];

	const result = [];
	const length = array.length;
	let i = 0;
	while (i < length) {
		result.push(slice(array, i, i += count));
	}
	return result;
};
