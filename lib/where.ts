import filter from './filter';
import matcher from './matcher';

// Convenience version of a common use case of `filter`: selecting only objects
// containing specific `key:value` pairs.
export default function where(obj, attrs): any[] {
	return filter(obj, matcher(attrs));
}
