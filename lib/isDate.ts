import isObjectLike from './isObjectLike';
import toType from './_toType';

export default function isDate(obj) {
	return isObjectLike(obj) && toType(obj) === 'Date';
}
