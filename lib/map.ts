import cb from './_cb';
import keys from './keys';
import values from './values';
import isArrayLike from './isArrayLike';

// Return the results of applying the iteratee to each element.
export default function map(obj, iteratee, context?) {
	iteratee = cb(iteratee, context);
	const names = !isArrayLike(obj) && keys(obj),
		length = (names || obj).length,
		results = Array(length);
	for (let index = 0; index < length; index++) {
		const name = names ? names[index] : index;
		results[index] = iteratee(obj[name], name, obj);
	}
	return results;
}
