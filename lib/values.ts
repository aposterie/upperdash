import keys from './keys';

// Retrieve the values of an object's properties.
export default function values(obj: Object): any[] {
	const names = keys(obj);
	const length = names.length;
	const values = Array(length);
	for (let i = 0; i < length; i++) {
		values[i] = obj[names[i]];
	}
	return values;
}
