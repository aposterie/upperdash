const placeholders = {};

Object.defineProperty(placeholders, 'toString', {
	value: () => 'undefined'
});

export default Object.freeze(placeholders);
