import slice from './slice';
import isFunction from './isFunction';
import executeBound from './_executeBound';

// Create a function bound to a given object (assigning `this`, and arguments,
// optionally).
export default function bind(func: Function, context) {
	if (!isFunction(func)) {
		throw new TypeError('Bind must be called on a function');
	}

	const args = slice(arguments, 2);
	return function bound() {
		return executeBound(
			func, bound, context, this,
			args.concat(slice(arguments))
		);
	};
}
