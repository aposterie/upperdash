export default function property(key: number|string|symbol) {
	return function (obj?: Object) {
		return obj == null ? undefined : obj[key];
	};
}
