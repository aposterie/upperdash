import cb from './_cb';
import map from './map';
import pluck from './pluck';

// Sort the object's values by a criterion produced by an iteratee.
export default function sortBy(obj, iteratee, context?) {
	iteratee = cb(iteratee, context);
	return pluck(map(obj, (value, index, list) => ({
		value: value,
		index: index,
		criteria: iteratee(value, index, list)
	})).sort(function (left, right) {
		const a = left.criteria;
		const b = right.criteria;
		if (a !== b) {
			if (a > b || a === undefined) return 1;
			if (a < b || b === undefined) return -1;
		}
		return left.index - right.index;
	}), 'value');
}
