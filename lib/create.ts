import baseCreate from './_baseCreate';
import extendOwn from './extendOwn';

// Creates an object that inherits from the given prototype object.
// If additional properties are provided then they will be added to the
// created object.
export default function create(prototype, props) {
	let result = baseCreate(prototype);
	if (props) extendOwn(result, props);
	return result;
}
