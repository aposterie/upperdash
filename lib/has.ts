const hasOwnProperty = Object.prototype.hasOwnProperty;

// Shortcut function for checking if an object has a given property directly
// on itself (in other words, not on a prototype).
export default function has(obj: Object, key: string|symbol): boolean {
	return obj != null && hasOwnProperty.call(obj, key);
}
