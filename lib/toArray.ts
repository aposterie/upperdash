import slice from './slice';
import values from './values';
import isArrayLike from './isArrayLike';
import isString from './isString';

const reStrSymbol = /[^\ud800-\udfff]|[\ud800-\udbff][\udc00-\udfff]|[\ud800-\udfff]/g;

// Safely create a real, live array from anything iterable.
export default function toArray(obj: any): any[] {
	if (!obj) return [];
	if (isArrayLike(obj)) return slice(obj);
	if (isString(obj)) return obj.match(reStrSymbol);
	return values(obj);
}
