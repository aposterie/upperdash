import map from './map';
import property from './property';

// Convenience version of a common use case of `map`: fetching a property.
export default function pluck(obj, key: number|string|symbol) {
	return map(obj, property(key));
}
