import toType from './_toType';

// Is a given value a boolean?
export default function isBoolean(obj) {
	return obj === true || obj === false || toType(obj) === 'Boolean';
}
