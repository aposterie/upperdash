// Predicate-generating functions. Often useful outside of Underscore.
export default function constant<T>(value: T): () => T {
	return () => value;
}
