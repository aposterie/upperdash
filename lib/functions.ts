import isFunction from './isFunction';

// Return a sorted list of the function names available on the object.
// Aliased as `methods`
export default function functions(obj: Object): string[] {
	let names: string[] = [];
	for (let key in obj) {
		if (isFunction(obj[key])) names.push(key);
	}
	return names.sort();
}
