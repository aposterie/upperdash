import createEscaper from './_createEscaper';
import escapeMap from './_escapeMap';

export default createEscaper(escapeMap);
