import uniq from './uniq';
import baseFlatten from './_baseFlatten';

// Produce an array that contains the union: each distinct element from all of
// the passed-in arrays.

export default function union(...args: any[]): any[];

export default function union() {
	return uniq(baseFlatten(arguments, true, true));
}
