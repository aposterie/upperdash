import random from './random';
import values from './values';
import isArrayLike from './isArrayLike';

// Shuffle a collection, using the modern version of the
// Fisher-Yates shuffle.
export default function shuffle(obj) {
	const set = isArrayLike(obj) ? obj : values(obj);
	const length = set.length;
	const shuffled = Array(length);
	for (let index = 0, rand; index < length; index++) {
		rand = random(0, index);
		if (rand !== index) shuffled[index] = shuffled[rand];
		shuffled[rand] = set[index];
	}
	return shuffled;
}
