import baseFlatten from './_baseFlatten';

// Flatten out an array, either recursively (by default), or just one level.
export default function flatten(array, shallow) {
	return baseFlatten(array, shallow, false);
}
