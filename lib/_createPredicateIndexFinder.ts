import cb from './_cb';
import getLength from './_getLength';

// Generator function to create the findIndex and findLastIndex functions
export default function createPredicateIndexFinder(dir: number) {
	return function (array: any[], predicate: Function, context): number {
		predicate = cb(predicate, context);
		const length = getLength(array);
		let index = dir > 0 ? 0 : length - 1;
		for (; index >= 0 && index < length; index += dir) {
			if (predicate(array[index], index, array)) return index;
		}
		return -1;
	};
}
