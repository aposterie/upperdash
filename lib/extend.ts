import createAssigner from './_createAssigner';
import allKeys from './allKeys';

// Extend a given object with all the properties in passed-in object(s).
export default createAssigner(allKeys);
