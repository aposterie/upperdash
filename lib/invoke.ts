import map from './map';
import slice from './slice';
import isFunction from './isFunction';

// Invoke a method (with arguments) on every item in a collection.
export default function invoke(obj, method) {
	const args = slice(arguments, 2);
	const isFunc = isFunction(method);
	return map(obj, (value) => {
		const func = isFunc ? method : value[method];
		return func == null ? func : func.apply(value, args);
	});
}
