import keys from './keys';
import optimizeCb from './_optimizeCb';
import isArrayLike from './isArrayLike';

// The cornerstone, an `each` implementation, aka `forEach`.
// Handles raw objects in addition to array-likes. Treats all
// sparse array-likes as if they were dense.
export default function each(obj, iteratee, context?) {
	iteratee = optimizeCb(iteratee, context);
	let i, length;
	if (isArrayLike(obj)) {
		for (i = 0, length = obj.length; i < length; i++) {
			iteratee(obj[i], i, obj);
		}
	} else {
		let names = keys(obj);
		for (i = 0, length = names.length; i < length; i++) {
			iteratee(obj[names[i]], names[i], obj);
		}
	}
	return obj;
}
