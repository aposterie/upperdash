import find from './find';
import matcher from './matcher';

// Convenience version of a common use case of `find`: getting the first object
// containing specific `key:value` pairs.
export default function findWhere(obj, attrs) {
	return find(obj, matcher(attrs));
}
