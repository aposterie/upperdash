import isObjectLike from './isObjectLike';
import toType from './_toType';

export default function isSet(obj: any): boolean {
	return isObjectLike(obj) && toType(obj) === 'Set';
}
