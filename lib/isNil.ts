// Is a given value equal to null or undefined?
export default function isNil(obj: any): boolean {
	return obj == null;
}
