// Is a given variable undefined?
export default function isUndefined(obj: any): boolean {
	return obj === undefined;
}
