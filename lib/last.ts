import rest from './rest';

// Get the last element of an array. Passing **n** will return the last N
// values in the array.
export default function last(array: any[], n: number, guard): any {
	if (array == null || array.length < 1) {
		return undefined;
	} else if (n == null || guard) {
		return array[array.length - 1];
	} else {
		return rest(array, Math.max(0, array.length - n));
	}
}
