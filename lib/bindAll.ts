import bind from './bind';

// Bind a number of an object's methods to that object. Remaining arguments
// are the method names to be bound. Useful for ensuring that all callbacks
// defined on an object belong to it.
export default function bindAll(obj) {
	let i, length = arguments.length, key;
	if (length <= 1) {
		throw new Error('bindAll must be passed function names');
	}
	
	for (i = 1; i < length; i++) {
		key = arguments[i];
		obj[key] = bind(obj[key], obj);
	}
	return obj;
}
