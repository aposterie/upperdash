// Is a given object a finite number?
// This is a polyfill for Number.isFinite;

if (typeof Number.isFinite !== 'function') {
	Object.defineProperty(Number, 'isFinite', {
		value: function (value) {
			return isFinite(value) && typeof value === 'number';
		}
	})
}

export default Number.isFinite;
