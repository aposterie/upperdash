import getLength from './_getLength';

const splice = [].splice;
const nativeLastIndexOf = [].lastIndexOf;

function pullNaN(array) {
	var index = getLength(array);
	while (--index) {
		if (isNaN(array[index])) {
			splice.call(array, index, 1);
		}
	}
}

export default function pullAll(array, values) {
	const length = getLength(values);
	let index = -1;
	let value, lastIndex;

	while (++index < length) {
		value = values[index];
		if (isNaN(value)) {
			pullNaN(array);
		} else {
			while ((lastIndex = nativeLastIndexOf.call(array, value)) > -1) {
				splice.call(array, lastIndex, 1);
			}
		}
	}
	
	return array;
}
