import before from './before';
import partial from './partial';

// Returns a function that will be executed at most one time, no matter how
// often you call it. Useful for lazy initialization.
export default partial(before, 2);
