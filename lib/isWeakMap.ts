import isObjectLike from './isObjectLike';
import toType from './_toType';

export default function isWeakMap(obj: any): boolean {
	return isObjectLike(obj) && toType(obj) === 'WeakMap';
}
