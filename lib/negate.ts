// Returns a negated version of the passed-in predicate.
export default function negate(predicate: Function): () => boolean {
	return function () {
		return !predicate.apply(this, arguments);
	};
}
