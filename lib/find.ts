import findKey from './findKey';
import findIndex from './findIndex';
import isArrayLike from './isArrayLike';

// Return the first value which passes a truth test. Aliased as `detect`.
export default function find(obj: any, predicate: Function, context?) {
	let key;
	if (isArrayLike(obj)) {
		key = findIndex(obj, predicate, context);
	} else {
		key = findKey(obj, predicate, context);
	}

	if (key !== undefined && key !== -1) {
		return obj[key];
	}
}
