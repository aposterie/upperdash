import slice from './slice';

// Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
// Especially useful on the arguments object. Passing an **n** will return
// the rest N values in the array.
export default function rest(array: any[], n?: number, guard?) {
	return slice(array, n == null || guard ? 1 : n);
}
