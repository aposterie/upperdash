import isObjectLike from './isObjectLike';
import toType from './_toType';

export default function isMap(obj: any): boolean {
	return isObjectLike(obj) && toType(obj) === 'Map';
}
