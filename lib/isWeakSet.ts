import isObjectLike from './isObjectLike';
import toType from './_toType';

export default function isWeakSet(obj: any): boolean {
	return isObjectLike(obj) && toType(obj) === 'WeakSet';
}
