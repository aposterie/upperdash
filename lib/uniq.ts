import cb from './_cb';
import contains from './contains';
import getLength from './_getLength';
import isBoolean from './isBoolean';

// Produce a duplicate-free version of the array. If the array has already
// been sorted, you have the option of using a faster algorithm.
// Aliased as `unique`.
export default function uniq<T>(array: T[], isSorted?: boolean, iteratee?, context?): T[] {
	if (!isBoolean(isSorted)) {
		context = iteratee;
		iteratee = isSorted;
		isSorted = false;
	}

	if (iteratee != null) {
		iteratee = cb(iteratee, context);
	}

	let result = [];
	let seen = [];

	for (let i = 0, length = getLength(array); i < length; i++) {
		const value = array[i];
		const computed = iteratee ? iteratee(value, i, array) : value;
		if (isSorted) {
			if (!i || seen !== computed) result.push(value);
			seen = computed;
		} else if (iteratee) {
			if (!contains(seen, computed)) {
				seen.push(computed);
				result.push(value);
			}
		} else if (!contains(result, value)) {
			result.push(value);
		}
	}
	return result;
}
