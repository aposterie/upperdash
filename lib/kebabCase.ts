import createCaseConverter from './_createCaseConverter';

export default createCaseConverter(letter => '-' + letter.toUpperCase());
