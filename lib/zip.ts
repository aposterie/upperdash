import unzip from './unzip';

// Zip together multiple lists into a single array -- elements that share
// an index go together.
export default function zip() {
	return unzip(arguments);
}
