import filter from './filter';

// Trim out all falsy values from an array.
export default function compact(array) {
	return filter(array, Boolean);
}
