import allKeys from './allKeys';
import createAssigner from './_createAssigner';

// Fill in a given object with default properties.
export default createAssigner(allKeys, true);
