import initial from './initial';

// Get the first element of an array. Passing **n** will return the first N
// values in the array. Aliased as `head` and `take`. The **guard** check
// allows it to work with `map`.
export default function first(array: any[], n?: number, guard?) {
	if (array == null || array.length < 1) return undefined;
	if (n == null || guard) return array[0];
	return initial(array, array.length - n);
}
