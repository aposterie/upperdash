import cb from './_cb';
import getLength from './_getLength';

// Use a comparator function to figure out the smallest index at which
// an object should be inserted so as to maintain order. Uses binary search.
export default function sortedIndex(array: any[], obj, iteratee: Function, context?) {
	iteratee = cb(iteratee, context, 1);
	const value = iteratee(obj);
	let low = 0, high = getLength(array);
	while (low < high) {
		const mid = Math.floor((low + high) / 2);
		if (iteratee(array[mid]) < value) {
			low = mid + 1;
		} else {
			high = mid;
		}
	}
	return low;
}
