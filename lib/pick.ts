import allKeys from './allKeys';
import isFunction from './isFunction';
import optimizeCb from './_optimizeCb';
import baseFlatten from './_baseFlatten';

// Return a copy of the object only containing the whitelisted properties.
export default function pick(object, oiteratee, context?) {
	let result = {}, obj = object, iteratee, keys;
	if (obj == null) {
		return result;
	} else if (isFunction(oiteratee)) {
		keys = allKeys(obj);
		iteratee = optimizeCb(oiteratee, context);
	} else {
		keys = baseFlatten(arguments, false, false, 1);
		iteratee = function (value, key, obj) {
			return key in obj;
		};
		obj = Object(obj);
	}
	for (let i = 0, length = keys.length; i < length; i++) {
		let key = keys[i];
		let value = obj[key];
		if (iteratee(value, key, obj)) result[key] = value;
	}
	return result;
}
