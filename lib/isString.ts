import toType from './_toType';

export default function isString(obj: any): boolean {
	return typeof obj === 'string' || toType(obj) === 'String';
}
