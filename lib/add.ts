export default function add(augend: number, addend: number) {
	return (augend || 0) + (addend || 0);
}
