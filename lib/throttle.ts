import now from './now';

interface ThrottleOptions {
	leading?: boolean;
	trailing?: boolean;
}

// Returns a function, that, when invoked, will only be triggered at most once
// during a given window of time. Normally, the throttled function will run
// as much as it can, without ever going more than once per `wait` duration;
// but if you'd like to disable the execution on the leading edge, pass
// `{leading: false}`. To disable execution on the trailing edge, ditto.
export default function throttle<T>(func: () => T, wait: number, options: ThrottleOptions): () => T {
	let context, args, result;
	let timeout = null;
	let previous = 0;
	if (!options) options = {};
	function later() {
		previous = options.leading === false ? 0 : now();
		timeout = null;
		result = func.apply(context, args);
		if (!timeout) {
			context = args = null;
		}
	}
	return function () {
		const time = now();
		if (!previous && options.leading === false) {
			previous = time;
		}
		const remaining = wait - (time - previous);
		context = this;
		args = arguments;
		if (remaining <= 0 || remaining > wait) {
			if (timeout) {
				clearTimeout(timeout);
				timeout = null;
			}
			previous = time;
			result = func.apply(context, args);
			if (!timeout) context = args = null;
		} else if (!timeout && options.trailing !== false) {
			timeout = setTimeout(later, remaining);
		}
		return result;
	};
}
