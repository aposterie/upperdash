export default function isObjectLike(obj: any): boolean {
	return !!obj && typeof obj == 'object';
}
