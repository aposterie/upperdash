import isObject from './isObject';

// An internal function for creating a new object that inherits from another.
export default function baseCreate(prototype): Object {
	if (!isObject(prototype)) return {};
	return Object.create(prototype);
}
