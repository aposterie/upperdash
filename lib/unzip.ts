import max from './max';
import pluck from './pluck';
import getLength from './_getLength';

// Complement of zip. Unzip accepts an array of arrays and groups
// each array's elements on shared indices
export default function unzip(array: any[]): any[] {
	const length = array && max(array, getLength).length || 0;
	const result = Array(length);

	for (let index = 0; index < length; index++) {
		result[index] = pluck(array, index);
	}
	return result;
}
