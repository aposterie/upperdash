import values from './values';
import random from './random';
import shuffle from './shuffle';
import isArrayLike from './isArrayLike';

// Sample **n** random values from a collection.
// If **n** is not specified, returns a single random element.
// The internal `guard` argument allows it to work with `map`.
export default function sample(obj, n: number, guard?) {
	if (n == null || guard) {
		if (!isArrayLike(obj)) obj = values(obj);
		return obj[random(obj.length - 1)];
	}
	return shuffle(obj).slice(0, Math.max(0, n));
}
