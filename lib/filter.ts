import cb from './_cb';
import each from './each';

// Return all the elements that pass a truth test.
// Aliased as `select`.
export default function filter(obj, predicate, context?) {
	let results = [];
	predicate = cb(predicate, context);
	each(obj, function (value, index, list) {
		if (predicate(value, index, list)) results.push(value);
	});
	return results;
}
