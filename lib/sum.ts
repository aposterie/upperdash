import getLength from './_getLength';

export default function sum(array: number[]): number {
	var result = 0, index = -1, length: number = getLength(array);
	while (++index < length) {
		result += (array[index] || 0);
	}
	return result;
}
