import isArray from './isArray';
import isArrayLike from './isArrayLike';
import isArguments from './isArguments';
import getLength from './_getLength';

// Internal implementation of a recursive `flatten` function.
export default function baseFlatten(input, shallow, strict, startIndex?: number) {
	let output = [], idx = 0;
	for (let i = startIndex || 0, length = getLength(input); i < length; i++) {
		let value = input[i];
		if (isArrayLike(value) && (isArray(value) || isArguments(value))) {
			// baseFlatten current level of array or arguments object
			if (!shallow) {
				value = baseFlatten(value, shallow, strict);
			}

			let j = 0, len = value.length;
			output.length += len;
			while (j < len) {
				output[idx++] = value[j++];
			}
		} else if (!strict) {
			output[idx++] = value;
		}
	}
	return output;
}
