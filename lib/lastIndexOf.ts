import findLastIndex from './findLastIndex';
import createIndexFinder from './_createIndexFinder';

export default createIndexFinder(-1, findLastIndex);
