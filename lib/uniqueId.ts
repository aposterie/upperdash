// Generate a unique integer id (unique within the entire client session).
// Useful for temporary DOM ids.
let idCounter = 0;
export default function uniqueId(prefix?: string): string {
	let id = ++idCounter + '';
	return prefix ? prefix + id : id;
}
