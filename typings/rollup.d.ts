// Type definitions for rollup.js v0.25.4
// Project: https://github.com/rollup/rollup

interface Plugin {
	options?(options: RollupOptions);
	// load(id): string | undefined;
	intro?(): string;
	outro?(): string;
	banner?: string;
	footer?: string;
}

interface RollupOptions {
	entry: string;
	external?: string[];
	onwarn?: Function;
	plugins: Plugin[];
}

declare module 'rollup-plugin-typescript' {
	export default function (options?: Object): Plugin;
}

declare module 'rollup' {
	interface BundleOptions {
		format?: 'amd' | 'cjs' | 'es6' | 'iife' | 'umd';
		exports?: 'auto' | 'default' | 'named' | 'none';
		moduleId?: string;
		moduleName?: string;
		global?: {
			[key: string]: string;
		}
		indent?: string | boolean;
		banner?: string;
		footer?: string;
		intro?: string;
		outro?: string;
		sourceMap?: boolean;
		sourceMapFile?: string;
	}

	interface BundleWriteOptions extends BundleOptions {
		dest: string;
		sourceMap: boolean | 'inline';
		sourceMapFile: void;
	}

	interface Bundled {
		code: string;
		map?: {
			toString(): string;
			toUrl(): string;
		}
	}

	interface Bundle {
		generate(options: BundleOptions): Bundled;
		write(options: BundleWriteOptions): Promise<void>;
	}

	function rollup(options: RollupOptions): Promise<Bundle>;
}
