// upperdash.js
// MIT License.

// Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
// http://underscorejs.org
// (C) 2009-2016 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors

/* eslint-disable */
var __DEV__ = false;

function add(augend, addend) {
    return (augend || 0) + (addend || 0);
}

// Returns a function that will only be executed on and after the Nth call.
function after(times, func) {
    return function () {
        if (--times < 1) {
            return func.apply(this, arguments);
        }
    };
}

// Is a given variable an object?
function isObject(obj) {
    var type = typeof obj;
    return !!obj && (type === 'function' || type === 'object');
}

// Retrieve all the property names of an object.
function allKeys(obj) {
    if (!isObject(obj))
        return [];
    var keys = [];
    for (var key in obj)
        keys.push(key);
    return keys;
}

// Returns a function that will only be executed up to (but not including) the Nth call.
function before(times, func) {
    var memo;
    return function () {
        if (--times > 0) {
            memo = func.apply(this, arguments);
        }
        if (times <= 1)
            func = null;
        return memo;
    };
}

var slice = Function.prototype.call.bind([].slice);

function toType(obj) {
    return Object.prototype.toString.call(obj).slice(8, -1);
}

var isFunction = (typeof /./ != 'function' && typeof Int8Array != 'object')
    ? function (obj) { return typeof obj == 'function'; }
    : function (obj) { return toType(obj) === 'Function'; };

// An internal function for creating a new object that inherits from another.
function baseCreate(prototype) {
    if (!isObject(prototype))
        return {};
    return Object.create(prototype);
}

// Determines whether to execute a function as a constructor
// or a normal function with the provided arguments
function executeBound(sourceFunc, boundFunc, context, callingContext, args) {
    if (!(callingContext instanceof boundFunc)) {
        return sourceFunc.apply(context, args);
    }
    var self = baseCreate(sourceFunc.prototype);
    var result = sourceFunc.apply(self, args);
    if (isObject(result))
        return result;
    return self;
}

// Create a function bound to a given object (assigning `this`, and arguments,
// optionally).
function bind(func, context) {
    if (!isFunction(func)) {
        throw new TypeError('Bind must be called on a function');
    }
    var args = slice(arguments, 2);
    return function bound() {
        return executeBound(func, bound, context, this, args.concat(slice(arguments)));
    };
}

// Bind a number of an object's methods to that object. Remaining arguments
// are the method names to be bound. Useful for ensuring that all callbacks
// defined on an object belong to it.
function bindAll(obj) {
    var i, length = arguments.length, key;
    if (length <= 1) {
        throw new Error('bindAll must be passed function names');
    }
    for (i = 1; i < length; i++) {
        key = arguments[i];
        obj[key] = bind(obj[key], obj);
    }
    return obj;
}

// String Functions
// -----------------
var nonWordChars = '([^\\w]|_)';
var caseConversionRegEx = RegExp(nonWordChars + '(\\w)', 'g');
function trimNonWordChars(str) {
    return ('' + str)
        .replace(RegExp('^' + nonWordChars + '+'), '')
        .replace(RegExp(nonWordChars + '+$'), '');
}
function createCaseConverter(replacer) {
    return function (str) {
        if (!str)
            return '';
        return trimNonWordChars(str)
            .replace(caseConversionRegEx, function (match, dash, letter) {
            return replacer(letter);
        });
    };
}

var camelCase = createCaseConverter(function (letter) { return letter.toUpperCase(); });

function chunk(array, count) {
    if (count == null || count < 1)
        return [];
    var result = [];
    var length = array.length;
    var i = 0;
    while (i < length) {
        result.push(slice(array, i, i += count));
    }
    return result;
}
;

var isArray = Array.isArray;

function createAssigner(keysFunc, defaults) {
    return function (obj) {
        var length = arguments.length;
        if (defaults)
            obj = Object(obj);
        if (length < 2 || obj == null)
            return obj;
        for (var index = 1; index < length; index++) {
            var source = arguments[index], keys = keysFunc(source), l = keys.length;
            for (var i = 0; i < l; i++) {
                var key = keys[i];
                if (!defaults || obj[key] === void 0)
                    obj[key] = source[key];
            }
        }
        return obj;
    };
}
;

var extend = createAssigner(allKeys);

// Create a (shallow-cloned) duplicate of an object.
function clone(obj) {
    if (!isObject(obj))
        return obj;
    return isArray(obj) ? slice.call(obj) : extend({}, obj);
}

var hasOwnProperty = Object.prototype.hasOwnProperty;
// Shortcut function for checking if an object has a given property directly
// on itself (in other words, not on a prototype).
function has(obj, key) {
    return obj != null && hasOwnProperty.call(obj, key);
}

// Retrieve the names of an object's own properties.
function keys(obj) {
    if (!isObject(obj))
        return [];
    var keys = [];
    for (var key in obj) {
        if (has(obj, key)) {
            keys.push(key);
        }
    }
    return keys;
}

var extendOwn = createAssigner(keys);

// Returns whether an object has a given set of `key:value` pairs.
function isMatch(object, attrs) {
    var names = keys(attrs);
    var length = names.length;
    if (object == null)
        return !length;
    var obj = Object(object);
    for (var i = 0; i < length; i++) {
        var key = names[i];
        if (attrs[key] !== obj[key] || !(key in obj))
            return false;
    }
    return true;
}

// Returns a predicate for checking whether an object has a given set of
// `key:value` pairs.
function matcher(attrs) {
    attrs = extendOwn({}, attrs);
    return function (obj) {
        return isMatch(obj, attrs);
    };
}

// Keep the identity function around for default iteratees.
function identity(value) {
    return value;
}

function property(key) {
    return function (obj) {
        return obj == null ? undefined : obj[key];
    };
}

// Internal function that returns an efficient (for current engines) version
// of the passed-in callback, to be repeatedly applied in other Underscore
// functions.
function optimizeCb(func, context, argCount) {
    if (context === undefined)
        return func;
    switch (argCount == null ? 3 : argCount) {
        case 1: return function (value) {
            return func.call(context, value);
        };
        case 2: return function (value, other) {
            return func.call(context, value, other);
        };
        case 3: return function (value, index, collection) {
            return func.call(context, value, index, collection);
        };
        case 4: return function (accumulator, value, index, collection) {
            return func.call(context, accumulator, value, index, collection);
        };
        default: return function () {
            return func.apply(context, arguments);
        };
    }
}

// A mostly-internal function to generate callbacks that can be applied
// to each element in a collection, returning the desired result — either
// identity, an arbitrary callback, a property matcher, or a property accessor.
function cb(value, context, argCount) {
    if (value == null) {
        return identity;
    }
    else if (isFunction(value)) {
        return optimizeCb(value, context, argCount);
    }
    else if (isObject(value)) {
        return matcher(value);
    }
    else {
        return property(value);
    }
}

var getLength = property('length');

// Helper for collection methods to determine whether a collection
// should be iterated as an array or as an object
// Related: http://people.mozilla.org/~jorendorff/es6-draft.html#sec-tolength
// Avoids a very nasty iOS 8 JIT bug on ARM-64. #2094
var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
function isArrayLike(collection) {
    var length = getLength(collection);
    return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
}

// The cornerstone, an `each` implementation, aka `forEach`.
// Handles raw objects in addition to array-likes. Treats all
// sparse array-likes as if they were dense.
function each(obj, iteratee, context) {
    iteratee = optimizeCb(iteratee, context);
    var i, length;
    if (isArrayLike(obj)) {
        for (i = 0, length = obj.length; i < length; i++) {
            iteratee(obj[i], i, obj);
        }
    }
    else {
        var names = keys(obj);
        for (i = 0, length = names.length; i < length; i++) {
            iteratee(obj[names[i]], names[i], obj);
        }
    }
    return obj;
}

// Return all the elements that pass a truth test.
// Aliased as `select`.
function filter(obj, predicate, context) {
    var results = [];
    predicate = cb(predicate, context);
    each(obj, function (value, index, list) {
        if (predicate(value, index, list))
            results.push(value);
    });
    return results;
}

// Trim out all falsy values from an array.
function compact(array) {
    return filter(array, Boolean);
}

// Returns a function that is the composition of a list of functions, each
// consuming the return value of the function that follows.
function compose() {
    var args = arguments;
    var start = args.length - 1;
    return function () {
        var i = start;
        var result = args[start].apply(this, arguments);
        while (i--)
            result = args[i].call(this, result);
        return result;
    };
}

// Predicate-generating functions. Often useful outside of Underscore.
function constant(value) {
    return function () { return value; };
}

// Generator function to create the findIndex and findLastIndex functions
function createPredicateIndexFinder(dir) {
    return function (array, predicate, context) {
        predicate = cb(predicate, context);
        var length = getLength(array);
        var index = dir > 0 ? 0 : length - 1;
        for (; index >= 0 && index < length; index += dir) {
            if (predicate(array[index], index, array))
                return index;
        }
        return -1;
    };
}

var findIndex = createPredicateIndexFinder(1);

// Use a comparator function to figure out the smallest index at which
// an object should be inserted so as to maintain order. Uses binary search.
function sortedIndex(array, obj, iteratee, context) {
    iteratee = cb(iteratee, context, 1);
    var value = iteratee(obj);
    var low = 0, high = getLength(array);
    while (low < high) {
        var mid = Math.floor((low + high) / 2);
        if (iteratee(array[mid]) < value) {
            low = mid + 1;
        }
        else {
            high = mid;
        }
    }
    return low;
}

// Generator function to create the indexOf and lastIndexOf functions
function createIndexFinder(dir, predicateFind, sortedIndex) {
    return function (array, item, idx) {
        var i = 0, length = getLength(array);
        if (typeof idx == 'number') {
            if (dir > 0) {
                i = idx >= 0 ? idx : Math.max(idx + length, i);
            }
            else {
                length = idx >= 0 ? Math.min(idx + 1, length) : idx + length + 1;
            }
        }
        else if (sortedIndex && idx && length) {
            idx = sortedIndex(array, item);
            return array[idx] === item ? idx : -1;
        }
        if (item !== item) {
            idx = predicateFind(slice(array, i, length), isNaN);
            return idx >= 0 ? idx + i : -1;
        }
        for (idx = dir > 0 ? i : length - 1; idx >= 0 && idx < length; idx += dir) {
            if (array[idx] === item)
                return idx;
        }
        return -1;
    };
}

var indexOf = createIndexFinder(1, findIndex, sortedIndex);

// Retrieve the values of an object's properties.
function values(obj) {
    var names = keys(obj);
    var length = names.length;
    var values = Array(length);
    for (var i = 0; i < length; i++) {
        values[i] = obj[names[i]];
    }
    return values;
}

// Determine if the array or object contains a given item (using `===`).
// Aliased as `includes`.
function contains(obj, item, fromIndex, guard) {
    if (!isArrayLike(obj))
        obj = values(obj);
    if (typeof fromIndex != 'number' || guard)
        fromIndex = 0;
    return indexOf(obj, item, fromIndex) >= 0;
}

// An internal function used for aggregate "group by" operations.
function group(behavior) {
    return function (obj, iteratee, context) {
        var result = {};
        iteratee = cb(iteratee, context);
        each(obj, function (value, index) {
            var key = iteratee(value, index, obj);
            behavior(result, value, key);
        });
        return result;
    };
}

var countBy = group(function (result, value, key) {
    if (has(result, key)) {
        result[key]++;
    }
    else {
        result[key] = 1;
    }
});

// Creates an object that inherits from the given prototype object.
// If additional properties are provided then they will be added to the
// created object.
function create(prototype, props) {
    var result = baseCreate(prototype);
    if (props)
        extendOwn(result, props);
    return result;
}

var now = Date.now;

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
    var timeout, args, context, timestamp, result;
    function later() {
        var last = now() - timestamp;
        if (last < wait && last >= 0) {
            timeout = setTimeout(later, wait - last);
        }
        else {
            timeout = null;
            if (!immediate) {
                result = func.apply(context, args);
                if (!timeout)
                    context = args = null;
            }
        }
    }
    return function () {
        context = this;
        args = arguments;
        timestamp = now();
        var callNow = immediate && !timeout;
        if (!timeout)
            timeout = setTimeout(later, wait);
        if (callNow) {
            result = func.apply(context, args);
            context = args = null;
        }
        return result;
    };
}

var defaults = createAssigner(allKeys, true);

// Delays a function for the given number of milliseconds, and then calls
// it with the arguments supplied.
function delay(func, wait) {
    var args = slice(arguments, 2);
    return setTimeout(function () {
        return func.apply(null, args);
    }, wait);
}

var placeholders = {};
Object.defineProperty(placeholders, 'toString', {
    value: function () { return 'undefined'; }
});
var defaultPlaceholder = Object.freeze(placeholders);

// Partially apply a function by creating a version that has had some of its
// arguments pre-filled, without changing its dynamic `this` context. _ acts
// as a placeholder, allowing any combination of arguments to be pre-filled.
function partial(func) {
    var boundArgs = slice(arguments, 1);
    var bound = function () {
        var position = 0;
        var length = boundArgs.length;
        var args = Array(length);
        var placeholder = partial['placeholder'] || defaultPlaceholder;
        for (var i = 0; i < length; i++) {
            args[i] = boundArgs[i] === placeholder ? arguments[position++] : boundArgs[i];
        }
        while (position < arguments.length)
            args.push(arguments[position++]);
        return executeBound(func, bound, this, this, args);
    };
    return bound;
}

var defer = partial(delay, defaultPlaceholder, 1);

function isObjectLike(obj) {
    return !!obj && typeof obj == 'object';
}

function isArguments(obj) {
    return isObjectLike(obj) && toType(obj) === 'Arguments';
}

// Internal implementation of a recursive `flatten` function.
function baseFlatten(input, shallow, strict, startIndex) {
    var output = [], idx = 0;
    for (var i = startIndex || 0, length = getLength(input); i < length; i++) {
        var value = input[i];
        if (isArrayLike(value) && (isArray(value) || isArguments(value))) {
            // baseFlatten current level of array or arguments object
            if (!shallow) {
                value = baseFlatten(value, shallow, strict);
            }
            var j = 0, len = value.length;
            output.length += len;
            while (j < len) {
                output[idx++] = value[j++];
            }
        }
        else if (!strict) {
            output[idx++] = value;
        }
    }
    return output;
}

function difference(array) {
    var rest = baseFlatten(arguments, true, true, 1);
    return filter(array, function (value) { return !contains(rest, value); });
}

// Functions for escaping and unescaping strings to/from HTML interpolation.
function createEscaper(map) {
    var escaper = function (match) { return map[match]; };
    // Regexes for identifying a key that needs to be escaped
    var source = '(?:' + keys(map).join('|') + ')';
    var testRegexp = RegExp(source);
    var replaceRegexp = RegExp(source, 'g');
    return function (string) {
        string = string == null ? '' : '' + string;
        return testRegexp.test(string)
            ? string.replace(replaceRegexp, escaper)
            : string;
    };
}

var escapeMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#x27;',
    '`': '&#x60;'
};

var _escape = createEscaper(escapeMap);

// Determine whether all of the elements match a truth test.
// Alased as `all`.
function every(obj, predicate, context) {
    predicate = cb(predicate, context);
    var names = !isArrayLike(obj) && keys(obj), length = (names || obj).length;
    for (var index = 0; index < length; index++) {
        var name = names ? names[index] : index;
        if (!predicate(obj[name], name, obj))
            return false;
    }
    return true;
}

// Returns the first key on an object that passes a predicate test
function findKey(obj, predicate, context) {
    predicate = cb(predicate, context);
    var names = keys(obj), name;
    for (var i = 0, length = names.length; i < length; i++) {
        name = names[i];
        if (predicate(obj[name], name, obj))
            return name;
    }
}

// Return the first value which passes a truth test. Aliased as `detect`.
function find(obj, predicate, context) {
    var key;
    if (isArrayLike(obj)) {
        key = findIndex(obj, predicate, context);
    }
    else {
        key = findKey(obj, predicate, context);
    }
    if (key !== undefined && key !== -1) {
        return obj[key];
    }
}

var findLastIndex = createPredicateIndexFinder(-1);

// Convenience version of a common use case of `find`: getting the first object
// containing specific `key:value` pairs.
function findWhere(obj, attrs) {
    return find(obj, matcher(attrs));
}

// Returns everything but the last entry of the array. Especially useful on
// the arguments object. Passing **n** will return all the values in
// the array, excluding the last N.
function initial(array, n, guard) {
    return slice(array, 0, Math.max(0, array.length - (n == null || guard ? 1 : n)));
}

// Get the first element of an array. Passing **n** will return the first N
// values in the array. Aliased as `head` and `take`. The **guard** check
// allows it to work with `map`.
function first(array, n, guard) {
    if (array == null || array.length < 1)
        return undefined;
    if (n == null || guard)
        return array[0];
    return initial(array, array.length - n);
}

// Flatten out an array, either recursively (by default), or just one level.
function flatten(array, shallow) {
    return baseFlatten(array, shallow, false);
}

// Return a sorted list of the function names available on the object.
// Aliased as `methods`
function functions(obj) {
    var names = [];
    for (var key in obj) {
        if (isFunction(obj[key]))
            names.push(key);
    }
    return names.sort();
}

var groupBy = group(function (result, value, key) {
    if (has(result, key)) {
        result[key].push(value);
    }
    else {
        result[key] = [value];
    }
});

var indexBy = group(function (result, value, key) {
    result[key] = value;
});

// Produce an array that contains every item shared between all the
// passed-in arrays.
function intersection(array) {
    var result = [];
    var argsLength = arguments.length;
    for (var i = 0, j = void 0, length = getLength(array); i < length; i++) {
        var item = array[i];
        if (contains(result, item))
            continue;
        for (j = 1; j < argsLength; j++) {
            if (!contains(arguments[j], item))
                break;
        }
        if (j === argsLength) {
            result.push(item);
        }
    }
    return result;
}

// Invert the keys and values of an object. The values must be serializable.
function invert(obj) {
    var result = {};
    var names = keys(obj);
    for (var i = 0, length = names.length; i < length; i++) {
        result[obj[names[i]]] = names[i];
    }
    return result;
}

// Return the results of applying the iteratee to each element.
function map(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    var names = !isArrayLike(obj) && keys(obj), length = (names || obj).length, results = Array(length);
    for (var index = 0; index < length; index++) {
        var name = names ? names[index] : index;
        results[index] = iteratee(obj[name], name, obj);
    }
    return results;
}

// Invoke a method (with arguments) on every item in a collection.
function invoke(obj, method) {
    var args = slice(arguments, 2);
    var isFunc = isFunction(method);
    return map(obj, function (value) {
        var func = isFunc ? method : value[method];
        return func == null ? func : func.apply(value, args);
    });
}

// Is a given value a boolean?
function isBoolean(obj) {
    return obj === true || obj === false || toType(obj) === 'Boolean';
}

function isDate(obj) {
    return isObjectLike(obj) && toType(obj) === 'Date';
}

// Is a given value a DOM element?
function isElement(obj) {
    return obj && obj.nodeType === 1;
}

function isString(obj) {
    return typeof obj === 'string' || toType(obj) === 'String';
}

// Is a given array, string, or object empty?
// An "empty" object has no enumerable own-properties.
function isEmpty(obj) {
    if (obj == null) {
        return true;
    }
    else if (isArrayLike(obj) && (isArray(obj) || isString(obj) || isArguments(obj))) {
        return obj.length === 0;
    }
    else {
        return keys(obj).length === 0;
    }
}

var toString = Object.prototype.toString;

function isEqual(a, b, aStack, bStack) {
    // Identical objects are equal. `0 === -0`, but they aren't identical.
    // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
    if (a === b)
        return a !== 0 || 1 / a === 1 / b;
    // A strict comparison is necessary because `null == undefined`.
    if (a == null || b == null)
        return a === b;
    // `NaN`s are equivalent, but non-reflexive.
    if (a !== a)
        return b !== b;
    // Exhaust primitive checks
    var type = typeof a;
    if (type !== 'function' && type !== 'object' && typeof b != 'object')
        return false;
    return deepEqual(a, b, aStack, bStack);
}
// Internal recursive comparison function for `isEqual`.
function deepEqual(a, b, aStack, bStack) {
    // Compare `[[Class]]` names.
    var className = toString.call(a);
    if (className !== toString.call(b))
        return false;
    className = className.slice(8, -1);
    switch (className) {
        // Strings, numbers, regular expressions, dates, and booleans are compared by value.
        case 'RegExp':
        // RegExps are coerced to strings for comparison (Note: '' + /a/i === '/a/i')
        case 'String':
            // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
            // equivalent to `new String("5")`.
            return '' + a === '' + b;
        case 'Number':
            // `NaN`s are equivalent, but non-reflexive.
            // Object(NaN) is equivalent to NaN.
            if (+a !== +a)
                return +b !== +b;
            // An `egal` comparison is performed for other numeric values.
            return +a === 0 ? 1 / +a === 1 / b : +a === +b;
        case 'Date':
        case 'Boolean':
            // Coerce dates and booleans to numeric primitive values. Dates are compared by their
            // millisecond representations. Note that invalid dates with millisecond representations
            // of `NaN` are not equivalent.
            return +a === +b;
        case 'Symbol':
            return Symbol.prototype.valueOf.call(a) === Symbol.prototype.valueOf.call(b);
    }
    var areArrays = className === '[object Array]';
    if (!areArrays) {
        if (typeof a != 'object' || typeof b != 'object')
            return false;
        // Objects with different constructors are not equivalent, but `Object`s or `Array`s
        // from different frames are.
        var aCtor = a.constructor, bCtor = b.constructor;
        if (aCtor !== bCtor
            && !(isFunction(aCtor) && aCtor instanceof aCtor &&
                isFunction(bCtor) && bCtor instanceof bCtor)
            && ('constructor' in a && 'constructor' in b)) {
            return false;
        }
    }
    // Assume equality for cyclic structures. The algorithm for detecting cyclic
    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.
    // Initializing stack of traversed objects.
    // It's done here since we only need them for objects and arrays comparison.
    aStack = aStack || [];
    bStack = bStack || [];
    var length = aStack.length;
    while (length--) {
        // Linear search. Performance is inversely proportional to the number of
        // unique nested structures.
        if (aStack[length] === a)
            return bStack[length] === b;
    }
    // Add the first object to the stack of traversed objects.
    aStack.push(a);
    bStack.push(b);
    // Recursively compare objects and arrays.
    if (areArrays) {
        // Compare array lengths to determine if a deep comparison is necessary.
        length = a.length;
        if (length !== b.length)
            return false;
        // Deep compare the contents, ignoring non-numeric properties.
        while (length--) {
            if (!isEqual(a[length], b[length], aStack, bStack))
                return false;
        }
    }
    else {
        // Deep compare objects.
        var names = keys(a);
        var key = void 0;
        length = names.length;
        // Ensure that both objects contain the same number of properties before comparing deep equality.
        if (keys(b).length !== length)
            return false;
        while (length--) {
            // Deep compare each member
            key = names[length];
            if (!(has(b, key) && isEqual(a[key], b[key], aStack, bStack)))
                return false;
        }
    }
    // Remove the first object from the stack of traversed objects.
    aStack.pop();
    bStack.pop();
    return true;
}

function isError(obj) {
    return isObjectLike(obj) && toType(obj) === 'Error';
}

// Is a given object a finite number?
// This is a polyfill for Number.isFinite;
if (typeof Number.isFinite !== 'function') {
    Object.defineProperty(Number, 'isFinite', {
        value: function (value) {
            return isFinite(value) && typeof value === 'number';
        }
    });
}
var _isFinite = Number.isFinite;

function isMap(obj) {
    return isObjectLike(obj) && toType(obj) === 'Map';
}

function isNumber(obj) {
    return typeof obj === 'number' || toType(obj) === 'Number';
}

// Is the given value `NaN`? (NaN is the only number which does not equal itself).
function _isNaN (obj) {
    return isNumber(obj) && isNaN(obj);
}

// Is a given value equal to null or undefined?
function isNil(obj) {
    return obj == null;
}

// Is a given value equal to null?
function isNull(obj) {
    return obj === null;
}

function isRegExp(obj) {
    return isObjectLike(obj) && toType(obj) === 'RegExp';
}

function isSet(obj) {
    return isObjectLike(obj) && toType(obj) === 'Set';
}

function isSymbol(obj) {
    return typeof obj === 'symbol' || toType(obj) === 'Symbol';
}

// Is a given variable undefined?
function isUndefined(obj) {
    return obj === undefined;
}

function isWeakMap(obj) {
    return isObjectLike(obj) && toType(obj) === 'WeakMap';
}

function isWeakSet(obj) {
    return isObjectLike(obj) && toType(obj) === 'WeakSet';
}

function iteratee(value, context) {
    return cb(value, context, Infinity);
}

var kebabCase = createCaseConverter(function (letter) { return '-' + letter.toUpperCase(); });

// Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
// Especially useful on the arguments object. Passing an **n** will return
// the rest N values in the array.
function rest(array, n, guard) {
    return slice(array, n == null || guard ? 1 : n);
}

// Get the last element of an array. Passing **n** will return the last N
// values in the array.
function last(array, n, guard) {
    if (array == null || array.length < 1) {
        return undefined;
    }
    else if (n == null || guard) {
        return array[array.length - 1];
    }
    else {
        return rest(array, Math.max(0, array.length - n));
    }
}

var lastIndexOf = createIndexFinder(-1, findLastIndex);

// Returns the results of applying the iteratee to each element of the object
// In contrast to `map` it returns an object
function mapObject(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    var names = keys(obj);
    var length = names.length;
    var results = {};
    for (var index = 0, name = void 0; index < length; index++) {
        name = names[index];
        results[name] = iteratee(obj[name], name, obj);
    }
    return results;
}

// Return the maximum element (or element-based computation).
function max(obj, iteratee, context) {
    var result = -Infinity, lastComputed = -Infinity, value, computed;
    if ((iteratee == null || typeof iteratee === 'number') && obj != null) {
        obj = isArrayLike(obj) ? obj : values(obj);
        for (var i = 0, length = obj.length; i < length; i++) {
            value = obj[i];
            if (value > result) {
                result = value;
            }
        }
    }
    else {
        iteratee = cb(iteratee, context);
        each(obj, function (value, index, list) {
            computed = iteratee(value, index, list);
            if (computed > lastComputed || computed === -Infinity && result === -Infinity) {
                result = value;
                lastComputed = computed;
            }
        });
    }
    return result;
}

// Memoize an expensive function by storing its results.
function memoize(func, hasher) {
    function memoized(key) {
        var cache = memoized['cache'];
        var address = '' + (hasher ? hasher.apply(this, arguments) : key);
        if (!has(cache, address)) {
            cache[address] = func.apply(this, arguments);
        }
        return cache[address];
    }
    memoized['cache'] = {};
    return memoized;
}

// Return the minimum element (or element-based computation).
function min(obj, iteratee, context) {
    var result = Infinity, lastComputed = Infinity, value, computed;
    if ((iteratee == null || typeof iteratee === 'number') && obj != null) {
        obj = isArrayLike(obj) ? obj : values(obj);
        var type = typeof obj[0];
        for (var i = 0, length = obj.length; i < length; i++) {
            value = obj[i];
            if (typeof value === type && value < result) {
                result = value;
            }
        }
    }
    else {
        iteratee = cb(iteratee, context);
        each(obj, function (value, index, list) {
            computed = iteratee(value, index, list);
            if (computed < lastComputed || computed === Infinity && result === Infinity) {
                result = value;
                lastComputed = computed;
            }
        });
    }
    return result;
}

// Returns a negated version of the passed-in predicate.
function negate(predicate) {
    return function () {
        return !predicate.apply(this, arguments);
    };
}

function noop() { }

function nthArg(num) {
    return function () {
        return arguments[num];
    };
}

// Converts lists into objects. Pass either a single array of `[key, value]`
// pairs, or two parallel arrays of the same length -- one of keys, and one of
// the corresponding values.
function object(list, values) {
    var result = {};
    for (var i = 0, length = getLength(list); i < length; i++) {
        if (values) {
            result[list[i]] = values[i];
        }
        else {
            result[list[i][0]] = list[i][1];
        }
    }
    return result;
}

// Return a copy of the object only containing the whitelisted properties.
function pick(object, oiteratee, context) {
    var result = {}, obj = object, iteratee, keys;
    if (obj == null) {
        return result;
    }
    else if (isFunction(oiteratee)) {
        keys = allKeys(obj);
        iteratee = optimizeCb(oiteratee, context);
    }
    else {
        keys = baseFlatten(arguments, false, false, 1);
        iteratee = function (value, key, obj) {
            return key in obj;
        };
        obj = Object(obj);
    }
    for (var i = 0, length = keys.length; i < length; i++) {
        var key = keys[i];
        var value = obj[key];
        if (iteratee(value, key, obj))
            result[key] = value;
    }
    return result;
}

// Return a copy of the object without the blacklisted properties.
function omit(obj, iteratee, context) {
    // Babel bug: https://phabricator.babeljs.io/T7198
    // TODO: Remove after Babel 6.7 release.
    var keys;
    if (isFunction(iteratee)) {
        iteratee = negate(iteratee);
    }
    else {
        keys = map(baseFlatten(arguments, false, false, 1), String);
        iteratee = function (value, key) {
            return !contains(keys, key);
        };
    }
    return pick(obj, iteratee, context);
}

var once = partial(before, 2);

// Convert an object into a list of `[key, value]` pairs.
function pairs(obj) {
    var names = keys(obj);
    var length = names.length;
    var pairs = Array(length);
    for (var i = 0; i < length; i++) {
        pairs[i] = [names[i], obj[names[i]]];
    }
    return pairs;
}

// Split a collection into two arrays: one whose elements all satisfy the given
// predicate, and one whose elements all do not satisfy the predicate.
function partition(obj, predicate, context) {
    predicate = cb(predicate, context);
    var pass = [], fail = [];
    each(obj, function (value, key, obj) {
        (predicate(value, key, obj) ? pass : fail).push(value);
    });
    return [pass, fail];
}

// Convenience version of a common use case of `map`: fetching a property.
function pluck(obj, key) {
    return map(obj, property(key));
}

// Generates a function for a given object that returns a given property.
function propertyOf(obj) {
    return obj == null
        ? constant(undefined)
        : function (key) { return obj[key]; };
}

var splice = [].splice;
var nativeLastIndexOf = [].lastIndexOf;
function pullNaN(array) {
    var index = getLength(array);
    while (--index) {
        if (isNaN(array[index])) {
            splice.call(array, index, 1);
        }
    }
}
function pullAll(array, values) {
    var length = getLength(values);
    var index = -1;
    var value, lastIndex;
    while (++index < length) {
        value = values[index];
        if (isNaN(value)) {
            pullNaN(array);
        }
        else {
            while ((lastIndex = nativeLastIndexOf.call(array, value)) > -1) {
                splice.call(array, lastIndex, 1);
            }
        }
    }
    return array;
}

function pull(array) {
    return pullAll(array, slice(arguments, 1));
}

// Return a random integer between min and max (inclusive).
function random(min, max) {
    if (max == null) {
        max = min;
        min = 0;
    }
    return min + Math.floor(Math.random() * (max - min + 1));
}

// Generate an integer Array containing an arithmetic progression. A port of
// the native Python `range()` function. See
// the Python documentation.
function range(start, stop, step) {
    if (stop == null) {
        stop = start || 0;
        start = 0;
    }
    step = step || 1;
    var length = Math.max(Math.ceil((stop - start) / step), 0);
    var range = Array(length);
    for (var idx = 0; idx < length; idx++, start += step) {
        range[idx] = start;
    }
    return range;
}

// Create a reducing function iterating left or right.
function createReduce(dir) {
    // Optimized iterator function as using arguments.length
    // in the main function will deoptimize the, see underscore #1991.
    function iterator(obj, iteratee, memo, keys, index, length) {
        for (; index >= 0 && index < length; index += dir) {
            var currentKey = keys ? keys[index] : index;
            memo = iteratee(memo, obj[currentKey], currentKey, obj);
        }
        return memo;
    }
    return function (obj, iteratee, memo, context) {
        iteratee = optimizeCb(iteratee, context, 4);
        var names = !isArrayLike(obj) && keys(obj);
        var length = (names || obj).length;
        var index = dir > 0 ? 0 : length - 1;
        // Determine the initial value if none is provided.
        if (arguments.length < 3) {
            memo = obj[names ? names[index] : index];
            index += dir;
        }
        return iterator(obj, iteratee, memo, names, index, length);
    };
}

var reduce = createReduce(1);

var reduceRight = createReduce(-1);

// Return all the elements for which a truth test fails.
function reject(obj, predicate, context) {
    return filter(obj, negate(cb(predicate)), context);
}

// If the value of the named `property` is a function then invoke it with the
// `object` as context; otherwise, return it.
function result(object, property, fallback) {
    var value = object == null ? undefined : object[property];
    if (value === undefined) {
        value = fallback;
    }
    return isFunction(value) ? value.call(object) : value;
}

// Shuffle a collection, using the modern version of the
// Fisher-Yates shuffle.
function shuffle(obj) {
    var set = isArrayLike(obj) ? obj : values(obj);
    var length = set.length;
    var shuffled = Array(length);
    for (var index = 0, rand = void 0; index < length; index++) {
        rand = random(0, index);
        if (rand !== index)
            shuffled[index] = shuffled[rand];
        shuffled[rand] = set[index];
    }
    return shuffled;
}

// Sample **n** random values from a collection.
// If **n** is not specified, returns a single random element.
// The internal `guard` argument allows it to work with `map`.
function sample(obj, n, guard) {
    if (n == null || guard) {
        if (!isArrayLike(obj))
            obj = values(obj);
        return obj[random(obj.length - 1)];
    }
    return shuffle(obj).slice(0, Math.max(0, n));
}

// Return the number of elements in an object.
function size(obj) {
    if (obj == null)
        return 0;
    return isArrayLike(obj) ? obj.length : keys(obj).length;
}

// custom: underscore-contrib | underscore.object.builders.js#L67
function snapshot(obj) {
    if (!isObject(obj))
        return obj;
    var temp = new obj.constructor();
    for (var key in obj) {
        if (has(obj, key))
            temp[key] = snapshot(obj[key]);
    }
    return temp;
}

// Determine if at least one element in the object matches a truth test.
// Aliased as `any`.
function some(obj, predicate, context) {
    predicate = cb(predicate, context);
    var names = !isArrayLike(obj) && keys(obj);
    var length = (names || obj).length;
    for (var index = 0; index < length; index++) {
        var currentKey = names ? names[index] : index;
        if (predicate(obj[currentKey], currentKey, obj))
            return true;
    }
    return false;
}

// Sort the object's values by a criterion produced by an iteratee.
function sortBy(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    return pluck(map(obj, function (value, index, list) { return ({
        value: value,
        index: index,
        criteria: iteratee(value, index, list)
    }); }).sort(function (left, right) {
        var a = left.criteria;
        var b = right.criteria;
        if (a !== b) {
            if (a > b || a === undefined)
                return 1;
            if (a < b || b === undefined)
                return -1;
        }
        return left.index - right.index;
    }), 'value');
}

function startsWith(string, target, position) {
    return string.substr(position || 0, target.length) === target;
}

function subtract(minuend, subtrahend) {
    return (minuend || 0) - (subtrahend || 0);
}

function sum(array) {
    var result = 0, index = -1, length = getLength(array);
    while (++index < length) {
        result += (array[index] || 0);
    }
    return result;
}

// Invokes interceptor with the obj, and then returns obj.
// The primary purpose of this method is to "tap into" a method chain, in
// order to perform operations on intermediate results within the chain.
function tap(obj, interceptor) {
    interceptor(obj);
    return obj;
}

var templateSettings = {
    evaluate: /<%([\s\S]+?)%>/g,
    interpolate: /<%=([\s\S]+?)%>/g,
    escape: /<%-([\s\S]+?)%>/g
};

// When customizing `templateSettings`, if you don't want to define an
// interpolation, evaluation or escaping regex, we need one that is
// guaranteed not to match.
var noMatch = /(.)^/;
// Certain characters need to be escaped so that they can be put into a
// string literal.
var escapes = {
    "'": "'",
    '\\': '\\',
    '\r': 'r',
    '\n': 'n',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
};
var escaper = /\\|'|\r|\n|\u2028|\u2029/g;
function escapeChar(match) {
    return '\\' + escapes[match];
}
// JavaScript micro-templating, similar to John Resig's implementation.
// Underscore templating handles arbitrary delimiters, preserves whitespace,
// and correctly escapes quotes within interpolated code.
// NB: `oldSettings` only exists for backwards compatibility.
function template(text, settings, oldSettings) {
    if (!settings && oldSettings)
        settings = oldSettings;
    settings = defaults({}, settings, templateSettings);
    // Combine delimiters into one regular expression via alternation.
    var matcher = RegExp([
        (settings.escape || noMatch).source,
        (settings.interpolate || noMatch).source,
        (settings.evaluate || noMatch).source
    ].join('|') + '|$', 'g');
    // Compile the template source, escaping string literals appropriately.
    var index = 0;
    var source = "__p+='";
    text.replace(matcher, function (match, escape, interpolate, evaluate, offset) {
        source += text.slice(index, offset).replace(escaper, escapeChar);
        index = offset + match.length;
        if (escape) {
            source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
        }
        else if (interpolate) {
            source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
        }
        else if (evaluate) {
            source += "';\n" + evaluate + "\n__p+='";
        }
        // Adobe VMs need the match returned to produce the correct offest.
        return match;
    });
    source += "';\n";
    // If a variable is not specified, place data values in local scope.
    if (!settings.variable) {
        source = 'with(obj||{}){\n' + source + '}\n';
    }
    source = "let __t,__p='',__j=Array.prototype.join," +
        "print=function(){__p+=__j.call(arguments,'');};\n" +
        source + 'return __p;\n';
    var render;
    try {
        render = new Function(settings.variable || 'obj', source);
    }
    catch (e) {
        e.source = source;
        throw e;
    }
    function template(data) {
        return render.call(this, data);
    }
    // Provide the compiled source as a convenience for precompilation.
    var argument = settings.variable || 'obj';
    template['source'] = 'function(' + argument + '){\n' + source + '}';
    return template;
}

// Returns a function, that, when invoked, will only be triggered at most once
// during a given window of time. Normally, the throttled function will run
// as much as it can, without ever going more than once per `wait` duration;
// but if you'd like to disable the execution on the leading edge, pass
// `{leading: false}`. To disable execution on the trailing edge, ditto.
function throttle(func, wait, options) {
    var context, args, result;
    var timeout = null;
    var previous = 0;
    if (!options)
        options = {};
    function later() {
        previous = options.leading === false ? 0 : now();
        timeout = null;
        result = func.apply(context, args);
        if (!timeout) {
            context = args = null;
        }
    }
    return function () {
        var time = now();
        if (!previous && options.leading === false) {
            previous = time;
        }
        var remaining = wait - (time - previous);
        context = this;
        args = arguments;
        if (remaining <= 0 || remaining > wait) {
            if (timeout) {
                clearTimeout(timeout);
                timeout = null;
            }
            previous = time;
            result = func.apply(context, args);
            if (!timeout)
                context = args = null;
        }
        else if (!timeout && options.trailing !== false) {
            timeout = setTimeout(later, remaining);
        }
        return result;
    };
}

// Run a function **n** times.
function times(n, iteratee, context) {
    var accum = Array(Math.max(0, n));
    iteratee = optimizeCb(iteratee, context, 1);
    for (var i = 0; i < n; i++) {
        accum[i] = iteratee(i);
    }
    return accum;
}

var reStrSymbol = /[^\ud800-\udfff]|[\ud800-\udbff][\udc00-\udfff]|[\ud800-\udfff]/g;
// Safely create a real, live array from anything iterable.
function toArray(obj) {
    if (!obj)
        return [];
    if (isArrayLike(obj))
        return slice(obj);
    if (isString(obj))
        return obj.match(reStrSymbol);
    return values(obj);
}

var unescapeMap = invert(escapeMap);
var _unescape = createEscaper(unescapeMap);

// Produce a duplicate-free version of the array. If the array has already
// been sorted, you have the option of using a faster algorithm.
// Aliased as `unique`.
function uniq(array, isSorted, iteratee, context) {
    if (!isBoolean(isSorted)) {
        context = iteratee;
        iteratee = isSorted;
        isSorted = false;
    }
    if (iteratee != null) {
        iteratee = cb(iteratee, context);
    }
    var result = [];
    var seen = [];
    for (var i = 0, length = getLength(array); i < length; i++) {
        var value = array[i];
        var computed = iteratee ? iteratee(value, i, array) : value;
        if (isSorted) {
            if (!i || seen !== computed)
                result.push(value);
            seen = computed;
        }
        else if (iteratee) {
            if (!contains(seen, computed)) {
                seen.push(computed);
                result.push(value);
            }
        }
        else if (!contains(result, value)) {
            result.push(value);
        }
    }
    return result;
}

function union() {
    return uniq(baseFlatten(arguments, true, true));
}

// Generate a unique integer id (unique within the entire client session).
// Useful for temporary DOM ids.
var idCounter = 0;
function uniqueId(prefix) {
    var id = ++idCounter + '';
    return prefix ? prefix + id : id;
}

// Complement of zip. Unzip accepts an array of arrays and groups
// each array's elements on shared indices
function unzip(array) {
    var length = array && max(array, getLength).length || 0;
    var result = Array(length);
    for (var index = 0; index < length; index++) {
        result[index] = pluck(array, index);
    }
    return result;
}

var version = "0.1.7";

// Convenience version of a common use case of `filter`: selecting only objects
// containing specific `key:value` pairs.
function where(obj, attrs) {
    return filter(obj, matcher(attrs));
}

// Return a version of the array that does not contain the specified value(s).
function without(array) {
    return difference(array, slice(arguments, 1));
}

// Returns the first function passed as an argument to the second,
// allowing you to adjust arguments, run code before and after, and
// conditionally execute the original function.
function wrap(func, wrapper) {
    return partial(wrapper, func);
}

// Zip together multiple lists into a single array -- elements that share
// an index go together.
function zip() {
    return unzip(arguments);
}

export { add, after, allKeys, before, bind, bindAll, camelCase, chunk, clone, compact, compose, constant, contains, contains as includes, countBy, create, debounce, defaults, defer, delay, difference, each, each as forEach, _escape as escape, every, every as all, extend, extendOwn, extendOwn as assign, filter, filter as select, find, find as detect, findIndex, findKey, findLastIndex, findWhere, first, first as head, first as take, flatten, functions, functions as methods, groupBy, has, identity, indexBy, indexOf, initial, intersection, invert, invoke, isArguments, isArray, isArrayLike, isBoolean, isDate, isElement, isEmpty, isEqual, isError, _isFinite as isFinite, isFunction, isMap, isMatch, _isNaN as isNaN, isNil, isNull, isNumber, isObject, isObjectLike, isRegExp, isSet, isString, isSymbol, isUndefined, isWeakMap, isWeakSet, iteratee, kebabCase, keys, last, lastIndexOf, map, map as collect, mapObject, matcher, matcher as matches, max, memoize, min, negate, noop, now, nthArg, object, omit, once, pairs, partial, partition, pick, defaultPlaceholder as placeholder, pluck, property, propertyOf, pull, pullAll, random, range, reduce, reduce as inject, reduce as foldl, reduceRight, reduceRight as foldr, reject, rest, rest as tail, rest as drop, result, sample, shuffle, size, slice, snapshot, snapshot as cloneDeep, some, some as any, sortBy, sortedIndex, startsWith, subtract, sum, tap, template, templateSettings, throttle, times, toArray, _unescape as unescape, union, uniq, uniq as unique, uniqueId, unzip, values, version, where, without, wrap, zip };