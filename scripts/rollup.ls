fs = require 'fs-promise'
ts = require 'typescript'
{ rollup } = require 'rollup'
typescript = require 'rollup-plugin-typescript'
{ minify } = require 'uglify-js'

root = "#{__dirname}/../"
banner = fs.read-file-sync "#{root}/static/banner.js", 'utf8'

require './create.ls'
.then ->
	# ES6 version
	rollup do
		entry: "#{root}/lib/upperdash.ts",
		plugins: [ typescript include: <[ **/*.ts ]> ]
.then ->
	Promise.all [
		it.write do
			format: 'es6'
			banner: banner.trim!
			dest: "#{root}/upperdash-es.js"
			intro: 'var __DEV__ = false;\n'
		it.write do
			format: 'umd'
			banner: banner.trim!
			dest: "#{root}/upperdash-umd.js"
			module-name: '_'
			intro: 'var __DEV__ = false;\n'
	]
.then ->
	fs.read-file "#{root}/upperdash-umd.js", 'utf8'
.then (src) ->
	# UMD minified version
	minified = minify src, from-string: true, mangle: true
	fs.write-file "#{root}/upperdash.min.js", minified.code
.then ->
	true
.catch ->
	console.warn it
