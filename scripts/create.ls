fs = require 'fs-promise'
alias = require '../alias.json'
{ version } = require '../package.json'

lib = "#{__dirname}/../lib"

create-version = ->
	fs.write-file "#{lib}/version.ts", "export default \"#{version}\";"

create-index = ->
	files <- fs.readdir lib .then

	modules = files
	.filter (-> not it.starts-with '_')
	.filter (.ends-with '.ts')
	.map (- /\.ts$/)

	dict = Object.create null

	for name in modules when name isnt 'upperdash'
		dict[name] = [name]

	for own name, aliases of alias
		dict[name] = dict[name].concat aliases

	src = [ "export { default as #{val} } from './#{name}';"  \
		for own name, aliases of dict for val in aliases ] * '\n'

	<- fs.write-file "#{lib}/upperdash.ts", src .then
	true

module.exports =
	create-version!.then create-index
