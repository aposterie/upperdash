# upperdash [![devDependency status](https://david-dm.org/alexlur/upperdash/status.svg)](https://david-dm.org/alexlur/upperdash#info=dependencies) [![npm version](https://img.shields.io/npm/v/upperdash.svg?style=flat)](https://www.npmjs.com/package/upperdash)

Underscore based utility belt designed for ES6 module loaders.

## Installation
To install upperdash with npm,

```bash
npm install --save upperdash
```

and to use it with an ES6 module loader such as [rollup.js](https://github.com/rollup/rollup), 
```javascript
import { assign, map, ... } from 'upperdash';
```

## Build
```bash
npm install && npm run build
```

## License
[MIT License](LICENSE).
